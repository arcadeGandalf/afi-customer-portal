﻿namespace Afi.DataAccess.Configurations;

using Afi.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).ValueGeneratedOnAdd();

        builder.Property(x => x.FirstName).HasMaxLength(50);
        builder.Property(x => x.Surname).HasMaxLength(50);
        builder.Property(x => x.ReferenceNumber).HasMaxLength(9);
        builder.Property(x => x.Email).HasMaxLength(320);
        builder.Property(x => x.DateOfBirth).IsRequired(false);
    }
}
