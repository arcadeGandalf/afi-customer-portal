﻿namespace Afi.DataAccess;

using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using System;

public class DesignTimeCacheContextFactory : IDesignTimeDbContextFactory<CustomerContext>
{
    public CustomerContext CreateDbContext(string[] args)
    {

        var folder = Environment.SpecialFolder.LocalApplicationData;
        var path = Environment.GetFolderPath(folder);
        var dbPath = Path.Join(path, "afi.db");

        var options = new DbContextOptionsBuilder<CustomerContext>();

        options.UseSqlite($"Data Source={dbPath}");

        return new CustomerContext(options.Options);
    }

}