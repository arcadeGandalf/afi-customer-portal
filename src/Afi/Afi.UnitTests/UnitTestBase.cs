﻿namespace Afi.UnitTests;

using Afi.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

public class UnitTestBase
{
    public CustomerContext Context { get; set; }

    public UnitTestBase()
    {
        var contextOptions = new DbContextOptionsBuilder<CustomerContext>()
           .UseInMemoryDatabase("CustomerContextTest")
           .ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning))
           .Options;

        Context = new CustomerContext(contextOptions);
    }
}
