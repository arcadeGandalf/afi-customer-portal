﻿namespace Afi.UnitTests.CommandHandlerTests;

using Afi.ApplicationServices.Commands;
using Xunit;

public  class CustomerCreateCommandHandlerTests : UnitTestBase
{
    private CustomerCreateCommandHandler _sut;

    public CustomerCreateCommandHandlerTests()
    {
        _sut = new CustomerCreateCommandHandler(Context);
    }

    [Fact]
    public async void Given_CustomerIsPassedIn_When_TheCommandIsCalled_Then_TheCustomerIsSaved()
    {
        // Arrange
        var command = new CustomerCreateCommand(
            "Bruce",
            "Wayne",
            "XX-398374",
            DateTime.UtcNow.AddYears(-40),
            "thebat@gmail.com");

        // Act
        await _sut.Handle(command, CancellationToken.None);

        // Assert
        Assert.True(Context.Customers.Any());
    }
}
