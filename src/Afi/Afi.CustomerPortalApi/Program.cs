using Afi.ApplicationServices.Commands;
using Afi.CustomerPortalApi.SwaggerExamples;
using Afi.DataAccess;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Filters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => c.ExampleFilters());

builder.Services.AddSwaggerExamplesFromAssemblyOf(typeof(CustomerCreateRequestExample));

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(CustomerCreateCommand).Assembly));

builder.Services.AddDbContext<CustomerContext>(options =>
{
    var folder = Environment.SpecialFolder.LocalApplicationData;
    var path = Environment.GetFolderPath(folder);
    var dbPath = Path.Join(path, "afi.db");

    options.UseSqlite($"Data Source={dbPath}");
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
