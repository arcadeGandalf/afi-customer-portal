﻿namespace Afi.CustomerPortalApi.Controllers;

using Afi.ApplicationServices.Commands;
using Afi.CustomerPortalApi.Models.Request;
using Afi.CustomerPortalApi.Models.Response;
using Afi.CustomerPortalApi.SwaggerExamples;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

[ApiController]
[Route("[controller]")]
public class CustomerController : ControllerBase
{
    private readonly IMediator _mediator;

    public CustomerController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    [ProducesResponseType(typeof(CustomerCreateResponseModel), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [SwaggerRequestExample(typeof(CustomerCreateRequestModel), typeof(CustomerCreateRequestExample))]
    public async Task<IActionResult> Create(CustomerCreateRequestModel requestModel)
    {
        var customer = await _mediator.Send(new CustomerCreateCommand(
            requestModel.FirstName,
            requestModel.Surname,
            requestModel.ReferenceNumber,
            requestModel.DateOfBirth,
            requestModel.Email));

        if (customer is null)
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return Ok(customer);
    }
}