﻿namespace Afi.CustomerPortalApi.Models.Response;

public class CustomerCreateResponseModel
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string Surname { get; set; }
    public string ReferenceNumber { get; set; }
    public DateTime DateOfBirth { get; set; }
    public string Email { get; set; }

    public CustomerCreateResponseModel(int id, string firstName, string surname,
        string referenceNumber, DateTime dateOfBirth, string email)
    {
        Id = id;
        FirstName = firstName;
        Surname = surname;
        ReferenceNumber = referenceNumber;
        DateOfBirth = dateOfBirth;
        Email = email;
    }
}
