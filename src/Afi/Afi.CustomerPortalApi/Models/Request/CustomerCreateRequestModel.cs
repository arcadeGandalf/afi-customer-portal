﻿namespace Afi.CustomerPortalApi.Models.Request;

using Afi.CustomerPortalApi.Validation;
using System.ComponentModel.DataAnnotations;

public class CustomerCreateRequestModel
{
    [MinLength(3)]
    [MaxLength(50)]
    public string FirstName { get; set; } = string.Empty;
    [MinLength(3)]
    [MaxLength(50)]
    public string Surname { get; set; } = string.Empty;
    [RegularExpression("^[A-Z]{2}-\\d{6}$", ErrorMessage = "Must match the format XX-999999")]
    public string ReferenceNumber { get; set; } = string.Empty;
    [DateOfBirth]
    [DateOfBirthOrEmail]
    public DateTime? DateOfBirth { get; set; }
    [RegularExpression("^[a-zA-Z0-9]{4,}@[a-zA-Z0-9]{2,}\\.(com|co\\.uk)$", ErrorMessage = "Email address is not valid.")]
    [DateOfBirthOrEmail]
    public string Email { get; set; } = string.Empty;
}
