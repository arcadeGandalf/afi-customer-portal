﻿namespace Afi.CustomerPortalApi.Validation;

using AgeCalculator;
using System.ComponentModel.DataAnnotations;

public class DateOfBirthAttribute : ValidationAttribute
{
    public string GetDateOfBirthAndEmailErrorMessage() =>
        $"Customer must be over 18.";

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var dateOfBirth = (DateTime?)value;

        if (dateOfBirth is null)
        {
            return ValidationResult.Success;
        }

        var ageDate = new Age((DateTime)dateOfBirth, DateTime.UtcNow);

        return ageDate.Years >= 18 ? ValidationResult.Success : new ValidationResult(GetDateOfBirthAndEmailErrorMessage());
    }
}
