﻿namespace Afi.CustomerPortalApi.Validation;

using Afi.CustomerPortalApi.Models.Request;
using System.ComponentModel.DataAnnotations;

public class DateOfBirthOrEmailAttribute : ValidationAttribute
{
    public string GetDateOfBirthErrorMessage() =>
        $"Date of birth or email is required.";

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var customerCreateRequestModel = validationContext.ObjectInstance as CustomerCreateRequestModel;

        if (customerCreateRequestModel is null) 
        {
            return null;
        }

        var dateOfBirthOrEmailSupplied = customerCreateRequestModel.DateOfBirth is not null || !string.IsNullOrEmpty(customerCreateRequestModel.Email);

        if (!dateOfBirthOrEmailSupplied)
        {
            return new ValidationResult(GetDateOfBirthErrorMessage());
        }

        return ValidationResult.Success;
    }
}
