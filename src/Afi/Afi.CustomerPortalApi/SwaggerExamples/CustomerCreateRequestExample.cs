﻿namespace Afi.CustomerPortalApi.SwaggerExamples;

using Afi.CustomerPortalApi.Models.Request;
using Swashbuckle.AspNetCore.Filters;

public class CustomerCreateRequestExample : IExamplesProvider<CustomerCreateRequestModel>
{
    public CustomerCreateRequestModel GetExamples()
    {
        return new CustomerCreateRequestModel
        {
            FirstName = "Bruce",
            Surname = "Wayne",
            ReferenceNumber = "BM-128746",
            DateOfBirth = DateTime.UtcNow.AddYears(-40),
            Email = "thebat@gmail.com"
        };
    }
}
