﻿namespace Afi.Domain;

public class Customer
{
    public int Id { get; }
    public string FirstName { get; internal set; }
    public string Surname { get; internal set; }
    public string ReferenceNumber { get; internal set; }
    public DateTime? DateOfBirth { get; internal set; }
    public string Email { get; internal set; }

    public Customer(int id, string firstName, string surname, string referenceNumber, DateTime? dateOfBirth, string email)
    {
        Id = id;
        FirstName = firstName;
        Surname = surname;
        ReferenceNumber = referenceNumber;
        DateOfBirth = dateOfBirth;
        Email = email;
    }

    public Customer(string firstName, string surname, string referenceNumber, DateTime? dateOfBirth, string email)
    {
        FirstName = firstName;
        Surname = surname;
        ReferenceNumber = referenceNumber;
        DateOfBirth = dateOfBirth;
        Email = email;
    }
}
