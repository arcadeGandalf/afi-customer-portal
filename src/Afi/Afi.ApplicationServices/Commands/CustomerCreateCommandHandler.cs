﻿namespace Afi.ApplicationServices.Commands;

using Afi.DataAccess;
using Afi.Domain;
using MediatR;

public class CustomerCreateCommandHandler : IRequestHandler<CustomerCreateCommand, Customer?>
{
    private readonly CustomerContext _context;

    public CustomerCreateCommandHandler(CustomerContext context)
    {
        _context = context;
    }

    public async Task<Customer?> Handle(CustomerCreateCommand request, CancellationToken cancellationToken)
    {
        var customer = new Customer(
            request.FirstName,
            request.Surname,
            request.ReferenceNumber,
            request.DateOfBirth,
            request.Email
            );

        _context.Add(customer);

        var rowsUpdated = await _context.SaveChangesAsync(cancellationToken);

        if (rowsUpdated == 1)
        {
            return customer;
        }

        return null;
    }
}
