﻿using Afi.Domain;
using MediatR;

namespace Afi.ApplicationServices.Commands;

public class CustomerCreateCommand : IRequest<Customer?>
{
    public CustomerCreateCommand(string firstName, string surname, string referenceNumber, DateTime? dateOfBirth, string email)
    {
        FirstName = firstName;
        Surname = surname;
        ReferenceNumber = referenceNumber;
        DateOfBirth = dateOfBirth;
        Email = email;
    }

    public string FirstName { get; }
    public string Surname { get; }
    public string ReferenceNumber { get; }
    public DateTime? DateOfBirth { get; }
    public string Email { get; }
}
