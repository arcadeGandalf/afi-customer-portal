# afi-customer-portal

## Tech
- Dotnet Core 8.0
- Entity Framework Core
- Sqlite
- Mediatr

## Setup
Ensure migrations have been run using the command `dotnet ef database update --project .\src\Afi\Afi.DataAccess\`.

## API usage
Either use Visual Studio to run the `Afi.CustomerPortalApi` project in the solution or run `dotnet run --project src\Afi\Afi.CustomerPortalApi\Afi.CustomerPortalApi.csproj`.

Navigate to the API URL: http://localhost:5200/swagger to test out the endpoint. There is example data prepopulated that will allow a record to pass validation. Alter these values to test out the validation for each property.

## Tests
An example unit test exists to show a typical Mediatr command/query test.
